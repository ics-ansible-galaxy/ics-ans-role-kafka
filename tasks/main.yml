---
- name: Install Python
  yum:
    name:
      - "{{ 'python' if ansible_os_family == 'RedHat' else 'python3' }}"
      - "{{ 'python-pip' if ansible_os_family == 'RedHat' else 'python3-pip' }}"

- name: Install kafka-python
  pip:
    name: kafka-python
    extra_args: "{{ kafka_pip_extra_args }}"

- name: Create kafka user
  user:
    name: kafka
    system: true
    home: "{{ kafka_dir }}"
    shell: /sbin/nologin

- name: Install Kafka
  unarchive:
    src: "{{ kafka_download_url }}"
    dest: "{{ kafka_dir }}"
    remote_src: true
    owner: kafka
    group: kafka
    creates: "{{ kafka_home }}"

- name: Configure logging for Kafka
  lineinfile:
    path: "{{ kafka_home }}/config/log4j.properties"
    regexp: "^{{ item.key }}="
    line: "{{ item.key }}={{ item.value }}"
  loop: "{{ kafka_log4j_config | dict2items }}"
  notify: restart Kafka

- name: Create configuration directories
  file:
    dest: "{{ item | dirname }}"
    state: directory
  loop:
    - "{{ kafka_properties_file }}"
    - "{{ kafka_zookeeper_properties_file }}"

- name: Create Zookeeper data directory
  file:
    dest: "{{ kafka_zookeeper_data_dir }}"
    state: directory
    owner: kafka
    group: kafka

- name: Create Kafka log directory
  file:
    dest: "{{ kafka_log_dirs }}"
    state: directory
    owner: kafka
    group: kafka

- name: Create Kafka properties file
  template:
    src: server.properties.j2
    dest: "{{ kafka_properties_file }}"
    owner: root
    group: root
    mode: 0644
  notify: restart Kafka

- name: Create Zookeeper properties file
  template:
    src: zookeeper.properties.j2
    dest: "{{ kafka_zookeeper_properties_file }}"
    owner: root
    group: root
    mode: 0644
  notify: restart Zookeeper

- name: Create JMX Exporter directory
  file:
    dest: "{{ kafka_jmx_exporter_dir }}"
    state: directory

- name: Install JMX exporter
  get_url:
    url: "{{ kafka_jmx_exporter_url }}"
    dest: "{{ kafka_jmx_exporter_dir }}"

- name: Create JMX Exporter configurations
  copy:
    src: "{{ item }}"
    dest: "{{ kafka_jmx_exporter_dir }}"
    mode: 0644
  loop:
    - kafka-jmx.yml
    - zookeeper-jmx.yml

- name: Install systemd services
  template:
    src: "{{ item }}.service.j2"
    dest: /etc/systemd/system/{{ item }}.service
    owner: root
    group: root
    mode: 0644
  loop:
    - kafka
    - zookeeper
  notify:
    - restart Zookeeper
    - restart Kafka

- name: Enable and start services
  systemd:
    name: "{{ item }}.service"
    daemon_reload: true
    enabled: true
    state: started
  loop:
    - zookeeper
    - kafka

- name: Wait for broker(s) to start
  command: "{{ kafka_home }}/bin/zookeeper-shell.sh localhost:2181 ls /brokers/ids"
  register: kafka_broker_ids
  changed_when: false
  until:
    - kafka_broker_ids.stdout is search("\[.+\]")
  retries: 10
  delay: 3

- name: Create Kafka ACLs
  kafka_acl:
    topic: "{{ item.topic | default(omit) }}"
    cluster: "{{ item.cluster | default(omit) }}"
    principal: "{{ item.principal | default(omit) }}"
    host: "{{ item.host | default(omit) }}"
    operation: "{{ item.operation | default(omit) }}"
    permission: "{{ item.permission | default(omit) }}"
    state: "{{ item.state | default(omit) }}"
  loop: "{{ kafka_acls }}"
