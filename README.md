# ics-ans-role-kafka

Ansible role to install Kafka.

## Role Variables

```yaml
kafka_scala_version: 2.12
kafka_version: 2.3.0
kafka_download_url: http://artifactory.esss.lu.se/artifactory/swi-pkg/apache/kafka/{{ kafka_version }}/kafka_{{ kafka_scala_version }}-{{ kafka_version }}.tgz
kafka_dir: /opt/kafka
kafka_user: kafka
kafka_group: kafka
kafka_properties_file: /etc/kafka/server.properties
kafka_auto_create_topics: true
kafka_log_dirs: /var/kafka-logs
kafka_zookeeper_properties_file: "/etc/zookeeper/zookeeper.properties"
kafka_zookeeper_data_dir: /var/zookeeper
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-kafka
```

## License

BSD 2-clause
