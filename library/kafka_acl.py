#!/usr/bin/python

# Copyright: (c) 2022, Anders Harrisson <anders.harrisson@ess.eu>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from ansible.module_utils.basic import AnsibleModule, missing_required_lib

try:
    from kafka.admin import (
        KafkaAdminClient,
        ACLPermissionType,
        ResourcePattern,
        ResourceType,
        ACL,
        ACLOperation,
    )
    from kafka.errors import NoError

    KAFKA_PYTHON_EXISTS = True
except ImportError:
    KAFKA_PYTHON_EXISTS = False


DOCUMENTATION = r"""
---
module: kafka_acl

short_description: Manage Kafka ACLs

version_added: "1.0.0"

description: Create or delete Kafka ACLs.

options:
    cluster:
        description: Cluster resource to add or remove ACLs to/from. Mutually exclusive with 'group' and 'topic'.
        required: false
        type: str
    group:
        description: Group resource to add or remove ACLs to/from. Mutually exclusive with 'cluster' and 'topic'.
        required: false
        type: str
    topic:
        description: Topic resource to add or remove ACLs to/from. Mutually exclusive with 'cluster' and 'group'.
        required: false
        type: str
    principal:
        description: Kafka principal, in the form 'User:<username>' to add or remove permissions for.
        required: true
        type: str
    host:
        description: Host or IP addresss to add or remove permissions for.
        required: false
        default: *
        type: str
    operation:
        description: Operation to allow or deny.
        required: false
        default: all
        type: str
        choices:
            - all
            - read
            - write
            - create
            - delete
            - alter
            - describe
            - cluster_action
            - describe_configs
            - alter_configs
            - idempotent_write
    permission:
        description: Allow or deny the defined ACL.
        required: false
        default: allow
        type: str
        choices:
            - allow
            - deny
    state:
        description: Create or delete the ACL.
        required: false
        default: present
        type: str
        choices:
            - present
            - absent

author:
    - Anders Harrisson
"""

EXAMPLES = r"""
- name: Create ACL
  kafka_acl:
    topic: "mytopic"
    principal: "User:johndoe"
    state: "present"

- name: Delete ACL
  kafka_acl:
    topic: "mytopic"
    principal: "User:johndoe"
    state: "absent"
"""


def run_module():

    # Parse arguments
    module_args = dict(
        topic=dict(),
        cluster=dict(),
        group=dict(),
        principal=dict(type="str", required=True),
        host=dict(type="str", default="*"),
        operation=dict(
            choices=[op.name.lower() for op in ACLOperation if op.name != "ANY"],
            default="all",
        ),
        permission=dict(
            choices=[
                permission.name.lower()
                for permission in ACLPermissionType
                if permission.name != "ANY"
            ],
            default="allow",
        ),
        state=dict(choices=["present", "absent"], default="present"),
    )

    # Create AnsibleModule
    module = AnsibleModule(
        argument_spec=module_args,
        mutually_exclusive=[("topic", "cluster", "group")],
        required_one_of=[("topic", "cluster", "group")],
        supports_check_mode=True,
    )

    # Verify that kafka-python is installed
    if not KAFKA_PYTHON_EXISTS:
        module.fail_json(msg=missing_required_lib("kafka-python"))

    # Default result
    result = dict(changed=False)

    # Parse resource argument
    if module.params["topic"] is not None:
        resource = ResourcePattern(ResourceType.TOPIC, module.params["topic"])
    elif module.params["cluster"] is not None:
        resource = ResourcePattern(ResourceType.CLUSTER, module.params["cluster"])
    elif module.params["Group"] is not None:
        resource = ResourcePattern(ResourceType.GROUP, module.params["group"])
    else:
        module.fail_json(msg="Incorrect resource type")

    # Create KafkaAdminClient
    admin_client = KafkaAdminClient()

    # Instantiate ACL object from module parameters
    acl = ACL(
        principal=module.params["principal"],
        host=module.params["host"],
        operation=ACLOperation[module.params["operation"].upper()],
        permission_type=ACLPermissionType[module.params["permission"].upper()],
        resource_pattern=resource,
    )

    # Describe ACL
    current_acls, error = admin_client.describe_acls(acl)
    if error != NoError:
        module.fail_json(msg="Could not describe ACL: {}".format(current_acls[1]))

    if not module.check_mode:
        # Create ACL
        if module.params["state"] == "present":
            acls_results = admin_client.create_acls([acl])
            if len(acls_results["failed"]) > 0:
                module.fail_json(
                    msg="Could not create ACL: {}".format(
                        str(acls_results["failed"][0][1])
                    )
                )
            result["changed"] = len(acls_results["succeeded"]) > len(current_acls)

        # Delete ACL
        if module.params["state"] == "absent":
            acls_results = admin_client.delete_acls([acl])
            if acls_results[0][2] != NoError:
                module.fail_json(
                    msg="Could not delete ACL: {}".format(acls_results[0][2])
                )
            result["changed"] = len(acls_results[0][1]) > 0

    module.exit_json(**result)


def main():
    run_module()


if __name__ == "__main__":
    main()
