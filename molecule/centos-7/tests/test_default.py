import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_services(host):
    assert host.service("kafka").is_running
    assert host.service("zookeeper").is_running


def test_sockets(host):
    assert host.socket("tcp://0.0.0.0:9092").is_listening
    assert host.socket("tcp://0.0.0.0:2181").is_listening


def test_acls(host):
    cmd = host.run('sudo /opt/kafka/kafka_2.13-3.6.2/bin/kafka-acls.sh --list --bootstrap-server localhost:9092')
    assert cmd.rc == 0
    assert cmd.stdout == (
        "Current ACLs for resource `ResourcePattern(resourceType=CLUSTER, name=kafka-cluster, patternType=LITERAL)`: \n"
        " 	(principal=User:*, host=*, operation=ALL, permissionType=ALLOW) \n"
        "\n"
        "Current ACLs for resource `ResourcePattern(resourceType=TOPIC, name=my_topic, patternType=LITERAL)`: \n"
        " 	(principal=User:*, host=127.0.0.1, operation=WRITE, permissionType=DENY) \n"
        "\n"
    )
